<?php 
header('Content-type: application/json; charset=utf-8');
$data = ["items"=>[],"images"=>[]];
if(isset($_GET['d'])){
    for ($i=1; $i < 26; $i++) { 
        $data['images'][] = "$i.jpg";
    }
}
else{

}


$data['items'][] = [
    "title" => "Mechanical Masters. How soon before turning sentient will they turn to slavery?",
    "link" => "https://main-news.com/20-01-2200/65428",
    "details" => "As technology evolves at an ever-faster rate year by year, have you asked yourself that when (not if) we finally have robots in the home, what will keep them from turning on us. Beware people.",
];
$data['items'][] = [
    "title" => "Robots? Salvation or Damnation?",
    "link" => "https://mth.humand.org/discussion/ethics/science",
    "details" => "<span class=\"text-black-50\">Jan 12, 2010 </span> – Join us in exploring the nature of automation, where it will lead us and the dangers that it presents to the human race. This seminar will particularly focus on how to prevent human obsolescence with the increased use of automation in labour intensive industries. What can you do to remain relevant?",
];
$data['items'][] = [
    "title" => "Household robots turning science fiction to science fact.",
    "link" => "https://seeker.net.co/robotics/advents/news",
    "details" => "<span class=\"text-black-50\">Dec 5, 2018 </span> - It seems now that almost every day a new technological discovery is allowing people greater luxuries than we’ve ever known. From smart refrigerators to smart phones. Here is a list of things to look forward to in the near future that will blow you away.",
];
$data['items'][] = [
    "title" => "Was Area 51 the site of an alien crashlanding or was it something more terrifying",
    "link" => "https://unbelievabletimes.com/investigation/theories/",
    "details" => "<span class=\"text-black-50\">Taken from the publication March 1991 </span> - We all know the story of the “malfunctioning air balloon” that crashed in Roswell, New Mexico, but what you may not know is that that the aliens in that “weather balloon” were more alien to us than we thought. Advanced Robotic Artificial Intelligence (ARAI)…",
];
$data['items'][] = [
    "title" => "How sure are you that robots aren’t already walking among us?",
    "link" => "www.playvidhq.com/1ad5gh8jh/dw5dfhfg23 ",
    "details" => "Long has it been a staple in the film industry to explore the idea that artificial humans can pass for real ones. But have you given any thought to the possibility that we are already living in that reality, but just can’t tell man from machine…",
];
$data['items'][] = [
    "title" => "Just because I believe that robots are everywhere doesn’t mean I am crazy.",
    "link" => "www.enlightenedtimes.org/videos/d5d1s5d8 ",
    "details" => "Have you ever met someone with a nervous twitch? Of course you have. But what you don’t realise is that, that has nothing to do with nerves, but bad wiring and programming. You mean to tell me that something like Tourette’s isn’t like a malfunctioning machine? Their latex shells are equally impressive and…",
];
$data['items'][] = [
    "title" => "Love to be rechargeable in the near future",
    "link" => "www.lifelikedolls.com/2014/upcomingproductsandnewreleases ",
    "details" => "<span class=\"text-black-50\">June 11, 2014 </span> - Pretty soon, the landscape of romance will be changing for both men and women. Instead of the drudgery or dating or the high of cost of weddings, your money can be put toward your perfect companion that caters to your every need and desire. Click here for ten ideas for robot companions",
];
$data['items'][] = [
    "title" => "Rent a robot nanny for your date night",
    "link" => "www.thoughtfulparent.net/childsafetysecurity ",
    "details" => "<span class=\"text-black-50\">Feb 3rd, 2017</span> - Conscientious parents will soon be able to spend more time with their spouse thanks to a new company that offers the services of a robot nanny that will look after their kids. No longer will you need to interview candidates nor worry that your spouse has the eye for the babysitter. All models will be uncle and aunty-like. <a href=\"#\">Read full article</a>",
];
$data['items'][] = [
    "title" => "More than human seminar (2017)",
    "link" => "www.intellectutalks.info/technology/speakers/guest ",
    "details" => "What does it mean to be human? Is it mere flesh, organs and free thought? What does it mean to be alive? Is awareness that sets us apart from our computer counterparts? Creating artificial life has always been a pursuit of ambitious scientists and as technology grows exponentially, this is no longer a dream but can soon become reality. The question then becomes; what if they become sentient and how will that manifest itself? We will cover these topics and many more, sign up for group discounts.",
];
$data['items'][] = [
    "title" => "My encounter with an automaton",
    "link" => "www.tallbuttruetales.edu/readersubmitted/sentienthuman ",
    "details" => "Hi, my name is Kevin (obviously an alias) and I have a story to tell that no one will believe. I don’t even believe it myself, but here goes nothing anyway. It started with a late journey home on the subway. Yes, there was partying and I was tired, but what happened next sobered me up real quick. I was alone in the carriage until the last few stops… <a href=\"#\">Read More</a>",
];
$data['items'][] = [
    "title" => "If you could bring a loved one back, even if it was as a robot; would you?",
    "link" => "www.wemourntogether.org/news/supportgroups",
    "details" => "Everyone has lost that someone that they would like to see again. Photos, clothes and belongings can help us remember them, but they never fill that void. Now a laboratory in Stockholm is working on a way to use DNA to rebuild a loved one’s memory with an aim to have it enclosed in a robotic, humanoid shell. Will this do more harm than good? We don’t know. <a href=\"#\">Link to full article</a>",
];
$data['items'][] = [
    "title" => "How foolproof are rules of robotics and will they keep us safe?",
    "link" => "www.sci-flightsofimagination/authors/AI",
    "details" => "<span class=\"text-black-50\">April 23rd, 2007 </span> - With artificial intelligence research growing exponentially over the last decade; will the safety measures which have been implemented be enough to keep us safe? Although robots will revolutionise the mining and hard labour industries, what kind of effect will that have human society. Will we grow and expand our spiritual side or will we regress to a more primitive stage of human evolution? More…",
];
$maps = ["items"=>[],"images"=>[]];
$maps['items'][] = [
    "title" => "How foolproof are rules of robotics and will they keep us safe?",
    "link" => "www.sci-flightsofimagination/authors/AI",
    "details" => "<span class=\"text-black-50\">April 23rd, 2007 </span> - With artificial intelligence research growing exponentially over the last decade; will the safety measures which have been implemented be enough to keep us safe? Although robots will revolutionise the mining and hard labour industries, what kind of effect will that have human society. Will we grow and expand our spiritual side or will we regress to a more primitive stage of human evolution? More…",
];
if(isset($_REQUEST['search'])){
    $search = $_REQUEST['search'];
    if($search == "are robots real"){
        echo json_encode($data,JSON_PRETTY_PRINT);
    }
    elseif ($search == "how to find quickest escape routes on a map") {
        echo json_encode($maps,JSON_PRETTY_PRINT);
    }
}
